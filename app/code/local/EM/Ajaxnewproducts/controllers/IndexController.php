<?php
class EM_Ajaxnewproducts_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$params = unserialize(base64_decode($this->getRequest()->getParam('params')));
		$curPage = $this->getRequest()->getParam('p');		
		$ajaxnewBlock = $this->getLayout()->createBlock('em_ajaxnewproducts/list')
										->setData($params)
										->setData('p',$curPage);
											
		$this->getResponse()->setBody($ajaxnewBlock->toHtml());
    }
}