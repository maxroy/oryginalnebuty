<?php
class EM_Ajaxnewproducts_Block_List extends Mage_Catalog_Block_Product_Abstract implements Mage_Widget_Block_Interface
{
	protected $_pageSize = 3;
	protected $_numRow = 3;
	protected $_maxPage = null;    
    protected function _construct()
    {
        parent::_construct();
        $cacheLifeTime = $this->getCacheLifeTime() ? $this->getCacheLifeTime() : 7200;
        $cacheTags = array(Mage_Catalog_Model_Product::CACHE_TAG,Mage_Cms_Model_Page::CACHE_TAG);
        if($this->ShowLabel() && Mage::helper('core')->isModuleEnabled('EM_Productlabels')){
            $cacheTags[] = EM_Productlabels_Model_Productlabels::CACHE_TAG;
        }
        $this->addData(array(
            'cache_lifetime'    => $cacheLifeTime,
            'cache_tags'        => $cacheTags
        ));
    }
    
    public function getCacheKeyInfo()
	{
		return array(
			'em_ajaxnewproducts',
			Mage::app()->getStore()->getId(),
			(int)Mage::app()->getStore()->isCurrentlySecure(),
			Mage::getDesign()->getPackageName(),
			Mage::getDesign()->getTheme('template'),
			Mage::app()->getStore()->getCurrentCurrencyCode(),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
			serialize($this->getData())
		);
	}   
	
	public function _prepareLayout()
	{
	
		return parent::_prepareLayout();
	}

	protected function _toHtml()
	{	
		if($this->getData('choose_template')	==	'custom_template')
		{
			if($this->getData('custom_theme'))
			$this->setTemplate($this->getData('custom_theme'));
			else
			$this->setTemplate('em_ajaxnewproducts/template_custom.phtml');
		}
		else
		{
			$this->setTemplate($this->getData('choose_template'));
		}
		return parent::_toHtml();
	}

	public function getCategories()
	{
		$strCategories=  $this->getData('new_category');
		$arrCategories = explode(",", $strCategories);
		return $arrCategories;
	}

	public function getColumnCount(){
		return $this->getData('column_count');
	}

	public function getCustomClass(){
		if ($this->getData('custom_class'))
		return $this->getData('custom_class');
	}

	public function getLimitCount(){
		if($this->getData('limit_count')==null)
		return 1;
		return $this->getData('limit_count');
	}

	public function getOrderBy(){
		return $this->getData('order_by');
	}

	public function getCacheLifeTime(){
		return $this->getData('cache_lifetime');
	}

    public function getThumbnailWidth(){
        $tempwidth = $this->getData('thumbnail_width');
        if (!(is_numeric($tempwidth)))
            $tempwidth = 150;
        return $tempwidth;
	}

    public function getThumbnailHeight(){
        $tempheight = $this->getData('thumbnail_height');
       if (!(is_numeric($tempheight)))
            $tempheight = 150;
        return $tempheight;
	}

	public function getItemWidth(){
        $tempwidth = $this->getData('item_width');
        if (!(is_numeric($tempwidth)))
            $tempwidth = null;
        return $tempwidth;
	}
    
    public function getItemHeight(){
        $tempheight = $this->getData('item_height');
       if (!(is_numeric($tempheight)))
            $tempheight = null;
        return $tempheight;
	}
	
	public function getItemSpacing(){
        $tempspacing= $this->getData('item_spacing');
       if (!(is_numeric($tempspacing)))
            $tempspacing = null;
        return $tempspacing;
	}	
	public function getItemClass(){
		if ($this->getData('item_class'))
		return $this->getData('item_class');
	}
	public function getFrontendTitle(){
		return $this->getData('frontend_title');
	}

	public function getFrontendDescription(){
		return $this->getData('frontend_description');
	}

	public function ShowThumb(){
		return $this->getData('show_thumbnail');
	}
    
    public function getAltImg(){
        return $this->getData('alt_img');
	}
    
    public function ShowProductName(){
        return $this->getData('show_product_name');
	}

	public function ShowDesc(){
		return $this->getData('show_description');
	}

	public function ShowPrice(){
		return $this->getData('show_price');
	}

	public function ShowReview(){
		return $this->getData('show_reviews');
	}

	public function ShowAddtoCart(){
		return $this->getData('show_addtocart');
	}

	public function ShowAddto(){
		return $this->getData('show_addto');
	}

	public function ShowLabel(){
		return $this->getData('show_label');
	}

	public function generateParamsWidget(){
		return base64_encode(serialize($this->getData()));
	}
	
	public function getIdJsWidget(){
		if(!$idJs = $this->getData('id_js'))
			$this->setData('id_js','em_ajaxnewproducts-'.rand(0,100));
		return $this->getData('id_js');	
	}
	
	public function getMaxPage(){
		return $this->_maxPage;
	}
	
	protected function getProductCollection()
	{
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		$products= Mage::getModel('catalog/product')->getCollection()
    		->addAttributeToFilter('status', array('neq' => Mage_Catalog_Model_Product_Status::STATUS_DISABLED))
    		->addAttributeToFilter('visibility',array("neq"=>1))
    		->addAttributeToFilter('news_from_date', array('or'=> array(
                0 => array('date' => true, 'to' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter('news_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
                    array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
                    )
              );
		//Sort		 
		$config2 = $this->getData('order_by');
        if(isset($config2))
		{      
           $orders = explode(' ',$config2);
        }
		if(count($orders))
            $products->addAttributeToSort($orders[0],$orders[1]);
		else
            $products->addAttributeToSort('name', 'asc');
            
		//Filter by categories
		$config1 = $this->getData('new_category');
		if($config1)
		{
			$result = array();
			$condition_cat = array();
			$alias = 'cat_index';
			$categoryCondition = $products->getConnection()->quoteInto(
			$alias.'.product_id=e.entity_id AND '.$alias.'.store_id=? AND ',
			$products->getStoreId()
			);
			$categoryCondition.= $alias.'.category_id IN ('.$config1.')';
			$products->getSelect()->joinInner(
			array($alias => $products->getTable('catalog/category_product_index')),
			$categoryCondition,
			array()
			);
			$products->_categoryIndexJoined = true;
			$products->distinct(true);
		}
		
		//Page size & CurPage
		$curPage = $this->getData('p');
		//$curPage = $this->getRequest()->getParam('p');
		$this->_pageSize = $this->getLimitCount();	
		$products->setPageSize($this->_pageSize);
		$products->setCurPage($curPage);
		$this->_maxPage = ceil($products->getSize()/$this->_pageSize);
		$products->addAttributeToSelect('*');

		$this->_addProductAttributesAndPrices($products);
		return $products;

	}
}
?>
