/**
 * Galathemes
 *
 * @license commercial software
 * @copyright (c) 2014 Codespot Software JSC - Galathemes.com. (http://www.galathemes.com)
 */

(function($) {

    if (typeof EM == 'undefined') EM = {};
    if (typeof EM.tools == 'undefined') EM.tools = {};
    
    
    var isMobile = /iPhone|iPod|iPad|Phone|Mobile|Android|hpwos/i.test(navigator.userAgent);
    var isPhone = /iPhone|iPod|Phone|Android/i.test(navigator.userAgent);
    
    
    var domLoaded = false, 
    	windowLoaded = false, 
    	last_adapt_i, 
    	last_adapt_width;
    /**
     * Fix iPhone/iPod auto zoom-in when text fields, select boxes are focus
     */
    function fixIPhoneAutoZoomWhenFocus() {
    	var viewport = $('head meta[name=viewport]');
    	if (viewport.length == 0) {
    		$('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0"/>');
    		viewport = $('head meta[name=viewport]');
    	}
    	
    	var old_content = viewport.attr('content');
    	
    	function zoomDisable(){
    		viewport.attr('content', old_content + ', user-scalable=0');
    	}
    	function zoomEnable(){
    		viewport.attr('content', old_content);
    	}
    	
    	$("input[type=text], textarea, select").mouseover(zoomDisable).mousedown(zoomEnable);
    };
    
    
    /**
     * Function called when layout size changed by adapt.js
     */
    function whenAdapt(i, width) {	
    	$('body').removeClass('adapt-0 adapt-1 adapt-2 adapt-3 adapt-4 adapt-5 adapt-6')
    		.addClass('adapt-'+i);
        if (FREEZED_TOP_MENU !=0 && isPhone == false){
        	var sticky_navigation = function(){
        		var scroll_top = $(window).scrollTop();
                if($('body').hasClass('adapt-0') == false){
                    if (scroll_top > 150) {
                        $('.em_area01').addClass('fixed-top');
            		} else {
                        $('.em_area01').removeClass('fixed-top');
            		} 
                }else {
                    $('.em_area01').removeClass('fixed-top');
        		}    		  
        	};
        	$(window).scroll(function() {
        		 sticky_navigation();
        	});
        }
        if($('body').hasClass('adapt-0') == true){
            $('.em_area01').removeClass('fixed-top');
        }
        setTimeout(function(){
    		if (typeof em_slider!=='undefined')
    			em_slider.reinit();
    	},100);
    };
    
    
    /**
     * Callback function called when stylesheet is changed by adapt.js
     */
    ADAPT_CONFIG.callback = function(i, width) {
    	last_adapt_i = i;
    	last_adapt_width = width;
    	
    	whenAdapt(last_adapt_i, last_adapt_width);
    };
    
    /**
    *   Add class mobile
    **/
    function addClassMobile(){
        if(isMobile == true){
            jQuery('body').addClass('mobile-view');
        }
    };
    
    $(document).ready(function() {
    	domLoaded = true;  
    	isMobile && fixIPhoneAutoZoomWhenFocus();
        handleBoxWide();
    	alternativeProductImage();
    	// safari hack: remove bold in h5, .h5
    	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
    		$('h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6').css('font-weight', 'normal');
    	}
    	addClassMobile();	
        backToTop();
        if(jQuery('body').viewPC()){
    		toolbar();
			/*$('#select-store').each(function() {
				$(this).insertUl();
				$(this).selectUl();
			});*/
    	}
        setupReviewLink();
        if (FREEZED_TOP_MENU !=0 && isPhone == false){persistentMenu();}    
        if(DETAILS_TAB != 0){decorateProductCollateralTabs();}
        toogleMyAccount();
        fixClickMobile();
        doAddtoButton(); 
        topSearch();   
    });
    
    $(window).bind('load', function() {
    	windowLoaded = true;
    	responsive();
    	whenAdapt(last_adapt_i, last_adapt_width);
    	doLanguageCurrency();	
        changeLanguageCode();
        changeCurrencyCode();        
        doSlider('#upsell-product-table',1,0,'horizontal');
        doSlider('#slider_crosell',1,0,'horizontal');
        doSlider('#slider_moreview',1,0,'horizontal');    
        doSlider('#slider_related',1,0,'vertical');
        initIsotope();
        setPositionItemCategory();    
    	toogleFooter();
    });
    
    $(window).bind('emadaptchange', function () {
        setPositionItemCategory();
        toogleFooter();
    });
    
    $(window).bind('orientationchange', function () {
        setPositionItemCategory();
    });

})(jQuery);

/**
 * Adjust elements to make it responsive
 *
 * Adjusted elements:
 * - Image of product items in products-grid scale to 100% width
 */
function responsive() {
    var $=jQuery;
	
	// resize products-grid's product image to full width 100% {{{
	var position = $('.products-grid .item').css('position');
	if (position != 'absolute' && position != 'fixed' && position != 'relative')
		$('.products-grid .item').css('position', 'relative');
	
	$('.item .product-image > img').each(function() {
		$(this).css({
			'max-width': '100%'
		});
	});
};

/**
 * Change the alternative product image when hover
 */
function alternativeProductImage() {
    var $=jQuery;
    var tm;
    function swap() {
        clearTimeout(tm);
        setTimeout(function() {
            el = $(this).find('img[data-alt-src]');
            var newImg = $(el).data('alt-src');
            var oldImg = $(el).attr('src');
            $(el).attr('src', newImg).data('alt-src', oldImg);
        }.bind(this), 200);
    }

    $('.item .product-image img[data-alt-src]').parents('.item').bind('mouseenter', swap).bind('mouseleave', swap);
};

function showAgreementPopup(e) {		
	jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').show()
		.css({
			'left': (parseInt(document.viewport.getWidth()) - jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').width())/2 +'px',
			'top': (parseInt(document.viewport.getHeight()) - jQuery('#checkout-agreements label.a-click').parent().parent().children('.agreement-content').height())/2 + 'px'
	});	
};

/**
 *   After Layer Update
 **/
window.afterLayerUpdate = function () {
    var $=jQuery;
    if($('body').viewPC()){
        toolbar();
    }
    initIsotope();
    alternativeProductImage();
    if (typeof EM_QUICKSHOP_DISABLED == 'undefined' || !EM_QUICKSHOP_DISABLED){    
        qs({
            itemClass: '.products-grid li.item, .products-list li.item, li.item .cate_product, .product-upsell-slideshow li.item, .mini-products-list li.item, #crosssell-products-list li.item', //selector for each items in catalog product list,use to insert quickshop image
            aClass: 'a.product-image', //selector for each a tag in product items,give us href for one product
            imgClass: '.product-image > img' //class for quickshop href
        });
    }      
};


function hideAgreementPopup(e) {
	jQuery('#checkout-agreements .agreement-content').hide();
	
};

function toolbar(){
    var $=jQuery;

    $('.show').each(function(){
        $(this).insertUl();
        $(this).selectUl();
    });
    $('.sortby').each(function(){
        $(this).insertUl();
        $(this).selectUl();
    });
};

// Back to top
function backToTop(){
    var $=jQuery;
    // hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('#back-top').fadeIn();
		} else {
			$('#back-top').fadeOut();
		}
	});

	// scroll body to 0px on click
	$('#back-top a').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});

};

/**
*   Toogle Footer Information Mobile View
**/
function toogleFooter(){
    if(isPhone==true || jQuery('body').hasClass('adapt-0') == true){
        jQuery('#footer-information ul').css('display','none');
        jQuery('#footer-information p.h5').addClass('toogle-icon');
        jQuery('#footer-information p.h5').unbind('click');
		jQuery('#footer-information p.h5').on('click', function(){
			jQuery(this).toggleClass("active").parent().find("ul").slideToggle();
		});		
    }else{
        jQuery('#footer-information p.h5').removeClass('toogle-icon');
        jQuery('#footer-information p.h5').removeClass('active');
        jQuery('#footer-information ul').css('display','block');
    }
};

function decorateProductCollateralTabs() {
    var $=jQuery;
    if($('.box-collateral').length > 1){
        $('.product-collateral').each(function(i) {
			$(this).wrap('<div class="tabs_wrapper_detail collateral_wrapper" />');
            $(this).prepend('<ul class="tabs_control"></ul>');
            $(this).children(".product-collateral-item").addClass("ui-slider-tabs-content-container");

			$('.box-collateral', this).addClass('tab-item').each(function(j) {
				var id = 'box_collateral_'+i+'_'+j;
				$(this).addClass('content_'+id);
                $(this).attr('id',id);
				$('.tabs_wrapper_detail ul.tabs_control').append('<li><a href="#'+id+'">'+$('h2', this).html()+'</a></li>');
			});
            $("div.tabs_wrapper_detail .product-collateral").sliderTabs();
		});
	
	}
};

function doAddtoButton(){	
	var $ = jQuery;    
    $('.products-grid').slideUpDown({
		divHover : '.hover-slide'
	});
};

/**
*   showReviewTab
**/
function showReviewList() {
	if (jQuery('#customer_review_list').size()) {
		// scroll to customer review
		jQuery('html, body').animate({ scrollTop: jQuery('#customer_review_list').offset().top }, 500);
	} else {
		return false;
	}
	return true;
};

function showReviewForm() {
	if (jQuery('#customer_review_form').size()) {
		// scroll to customer review
		jQuery('html, body').animate({ scrollTop: jQuery('#customer_review_form').offset().top }, 500);
	} else {
		return false;
	}
	return true;
};

function setupReviewLink() {
	jQuery('.product-view .product-essential .link_review_list').click(function (e) {
		if(showReviewList()){
            e.preventDefault();
		}
	});
    jQuery('.product-view .product-essential .link_review_form').click(function (e) {
		if(showReviewForm()){
            e.preventDefault();
		}
	});
};

function persistentMenu() {
	var $ = jQuery;
    var sticky_navigation = function(){
		var scroll_top = $(window).scrollTop();        
        if($('body').hasClass('adapt-0') == false){
            if (scroll_top > 150) {
                $('.em_area01').addClass('fixed-top');
    		} else {
                $('.em_area01').removeClass('fixed-top');
    		} 
        }else {
            $('.em_area01').removeClass('fixed-top');
		}    		  
	};
	$(window).scroll(function() {
		 sticky_navigation();
	});
    if($('body').hasClass('adapt-0') == true){
        $('.em_area01').removeClass('fixed-top');
    }
};

function doSlider($e, $move, $circular, $direction){
    if(jQuery($e + ' ul > li').size()>1){
        jQuery($e + ' > ul').addClass('slides');
        jQuery($e).csslider({
            move : $move,
            circular : $circular,
            direction : $direction,
            parentHidden : 'div.slider'
        });
    }
};

function toogleMyAccount(){
    var $=jQuery;
    $('.em-account').hide();
    /* Wishlist Link */ 
    if(isMobile == true){
        $("#link-account").attr("href","javascript:void(0)");
        $("#link-account").click(
            function( event ){
                $('.em-account').slideToggle();
            }
        );
    }else{
        var timeout1 = null;
        $('.account-link').mouseover(function(){            
            if (timeout1)
                clearTimeout(timeout1);
            timeout1 = setTimeout(function() {
                timeout1 = null;
                $('.em-account').slideDown('fast');
                $('.account-link').addClass('over');
            }, 200);
        });
        
        $('.account-link').mouseout(function(){
            if (timeout1)
                clearTimeout(timeout1);
            timeout1 = setTimeout(function() {
                timeout1 = null;
                $('.em-account').slideUp('fast');
                $('.account-link').removeClass('over');
            }, 200);
        });
    }
};

function initIsotope(){
	if(!isPhone){
    	jQuery.Isotope.prototype._getMasonryGutterColumns = function() {
    	    var gutter = this.options.masonry && this.options.masonry.gutterWidth || 0;
    	        containerWidth = this.element.width();
    	  
    	    this.masonry.columnWidth = this.options.masonry && this.options.masonry.columnWidth ||
    	                  // or use the size of the first item
    	                  this.$filteredAtoms.outerWidth(true) ||
    	                  // if there's no items, use size of container
    	                  containerWidth;
    
    	    this.masonry.columnWidth += gutter;
    
    	    this.masonry.cols = Math.floor( ( containerWidth + gutter ) / this.masonry.columnWidth );
    	    this.masonry.cols = Math.max( this.masonry.cols, 1 );
    	  };
    
    	  jQuery.Isotope.prototype._masonryReset = function() {
    	    // layout-specific props
    	    this.masonry = {};
    	    // FIXME shouldn't have to call this again
    	    this._getMasonryGutterColumns();
    	    var i = this.masonry.cols;
    	    this.masonry.colYs = [];
    	    while (i--) {
    	      this.masonry.colYs.push( 0 );
    	    }
    	  };
    
    	  jQuery.Isotope.prototype._masonryResizeChanged = function() {
    	    var prevSegments = this.masonry.cols;
    	    // update cols/rows
    	    this._getMasonryGutterColumns();
    	    // return if updated cols/rows is not equal to previous
    	    return ( this.masonry.cols !== prevSegments );
    	  };
		
		if(!jQuery('.category-products').hasClass('custom-store')){
			jQuery('.category-products ul.products-grid').isotope({
				itemSelector : '.item',
				masonry : {
				  },
				  layoutMode : PRODUCTSGRID_POSITION_ABSOLUTE,
			});
		}else{
			jQuery('.custom-store ul.products-grid').isotope({
				itemSelector : '.item',
				masonry : {
				  },
				  layoutMode : "masonry",
			});
		}
	}	
};
function topSearch(){
	var $=jQuery;
	if(isMobile == false){
        var timeout = null;		
        function hideSearch() {
        	if (timeout)
        	clearTimeout(timeout);
        	timeout = setTimeout(function() {
        		timeout = null;
                if ($('#search_autocomplete').css('display') == 'none') {
        			$('#content_search').slideUp();
        			$('#title_search').removeClass('over');
                }
        	}, 100);
        };
		
		function showSearch() {
			if (timeout)
			clearTimeout(timeout);
			timeout = setTimeout(function() {
			timeout = null;
			$('#content_search').slideDown();
			$('#title_search').addClass('over');
			}, 100);
		};
        
        $('#search_autocomplete ul').mouseover(function(){
            showSearch();
        });
                
        $('#search_autocomplete').click(function(){
            showSearch();
        });
        
        $('#content_search').mouseover(function(){
            showSearch();
        });
        
        $('#content_search').click(function(){
            showSearch();
        });
        
        $('#content_search').mouseout(function(){
            hideSearch();            
        });
        
        $('#title_search').mouseover(function(){
            showSearch();
        });
                
        $('#title_search').mouseout(function(){            
            hideSearch();
        });	
        
        var timeout = null;
	
    	// hide autocomplete when mouse out
        var timeout1 = null;
    	$('#search_autocomplete, .form-search').mouseleave(function () {
    		clearTimeout(timeout1);
    		timeout1 = setTimeout(function () {
    			$('#search_autocomplete').hide();
    		}, 100);
    	});
    	
    	$('#content_search').mouseleave(function () {
    		$('#content_search').slideUp();
    	});	
		
	}else{
	   var container = $("#content_search");
		$('#title_search').click(
			function( event ){
                event.preventDefault();
                container.slideToggle();
            }
		);
	}
};

function afterLoadAjax(id){
	responsive();
    doAddtoButton();
	if(!isPhone){
		if(PRODUCTSGRID_POSITION_ABSOLUTE=="fitRows"){
			jQuery(id + ' ul.products-grid .item').css('margin-right','10px');
		}
		var temp = jQuery(id + ' ul.products-grid li.item');	
	    temp.each(function(){
			if(!(jQuery(this).hasClass('isotope-item'))){
				jQuery(id + ' ul.products-grid').isotope( 'insert', jQuery(this));
			}
		});
	};    
    alternativeProductImage();
    if (typeof EM_QUICKSHOP_DISABLED == 'undefined' || !EM_QUICKSHOP_DISABLED){    
        qs({
            itemClass: '.products-grid li.item, .products-list li.item, li.item .cate_product, .product-upsell-slideshow li.item, .mini-products-list li.item, #crosssell-products-list li.item', //selector for each items in catalog product list,use to insert quickshop image
            aClass: 'a.product-image', //selector for each a tag in product items,give us href for one product
            imgClass: '.product-image > img' //class for quickshop href
        });
    }
};

function fixClickMobile(){
    var $=jQuery;
    var currentImage;
    var currentName;
    if (isMobile) {			
		$('.products-grid li.item a.product-image').bind('click', function(event) {
			if (currentImage != this) {
				event.preventDefault();
				// next time click this element will open link
				currentImage = this;
			}
		});
        $('.featured_category_content .grid_8 .box_banner > a').bind('click', function(event) {
			if (currentName != this) {
				event.preventDefault();
				// next time click this element will open link
				currentName = this;
			}
		});
	}
};

function doLanguageCurrency(){
    // Link Language Curency
    jQuery('#id_laguage_currency_content').hide();
    
     if(isMobile != true){
        jQuery('#id_language_currency_link')
         .bind('mouseover', showLanguageCurrency)
         .bind('click', showLanguageCurrency)
         .bind('mouseout', hideLanguageCurrency);
        
    }else{
        jQuery('#id_language_currency_link').click(function(){
            jQuery('#id_laguage_currency_content').slideToggle();
            jQuery('#id_language_currency_link').toggleClass('over');
        });
    }
    
    // My Language Curency
     jQuery('#id_laguage_currency_content')
     .bind('mouseover', showLanguageCurrency)
     .bind('click', showLanguageCurrency)
     .bind('mouseout', hideLanguageCurrency);
    
    // Hide Language Curency
    var timeoutlancur = null;
	function hideLanguageCurrency() {
		if (timeoutlancur)
		clearTimeout(timeoutlancur);
		timeoutlancur = setTimeout(function() {
		timeoutlancur = null;
		jQuery('#id_laguage_currency_content').slideUp();
		jQuery('#id_language_currency_link').removeClass('over');
		}, 200);
	}
    
    // Show Language Curency
	function showLanguageCurrency() {				
		if (timeoutlancur)
		clearTimeout(timeoutlancur);
		timeoutlancur = setTimeout(function() {
		timeoutlancur = null;
		jQuery('#id_laguage_currency_content').slideDown();
		jQuery('#id_language_currency_link').addClass('over');
		}, 200);
	};
	
    // Change Language
	jQuery('#language ul li a')  
		.bind('click', changeLanguageCode);
    
    // Change Currency
    jQuery('#currency ul.currency-content li a')  
		.bind('click', changeCurrencyCode);
    
        
};

// Function Change Language
function changeLanguageCode() {			
	link = jQuery('#language ul li.selected a').attr("href");
	/*jQuery('#language_link').attr("href",link);*/
	src = urlSkinsite + 'images/language/' + jQuery('#language ul li.selected a').attr("title") + '.png';
	jQuery('#language_link img').attr("src",src);
};

// Function Change Currency
function changeCurrencyCode() {			
	linkCurrency = jQuery('#currency ul.currency-content li.selected a').attr("href");
	/*jQuery('#currency_link').attr("href",linkCurrency);*/
    textCurrency = jQuery('#currency ul.currency-content li.selected a').attr("title");
    jQuery('#currency_link').text(textCurrency);
};

function setPositionItemCategory(){
    var $ = jQuery;
	
	$('.featured_category_content').find('.grid_8').each(function(){
        $('.box_banner', $(this)).each(function() {
            $(this).removeAttr('style').css('position','relative');
            var getImg = $(this).find('a');
            getImg.removeAttr('style').css('display','block');
            var getOffset = getImg.offset();
            var selector = $(this).find('.view_collection');
            selector.removeAttr('style');
            
    		var setTop = (getImg.height() - selector.outerHeight(true)+10)/2;
    		var setLeft = (getImg.width() - selector.outerWidth(true))/2;
    		selector.css({
    			'top': setTop+'px',
    			'left': setLeft+'px'
    		});
    	});
	});
};

function fixHomesTab(){
    if(jQuery('.emtabs .ui-slider-right-arrow').is(':hidden') && jQuery('.emtabs .ui-slider-left-arrow').is(':hidden')){
        jQuery('.emtabs .tabs_control').addClass('ul_no_tabs_slider');
        jQuery('.emtabs .ui-slider-tabs-list-container').addClass('wrapper_no_tabs_slider');        
    }else{
        jQuery('.emtabs .tabs_control').removeClass('ul_no_tabs_slider');
        jQuery('.emtabs .ui-slider-tabs-list-container').removeClass('wrapper_no_tabs_slider');  
    }
};

function handleBoxWide(){
    //box wide
    var $=jQuery;
    $('#em_box_wide').val(boxwide_selected);
    var mode = $('#em_box_wide').val();
    if(mode == 'box'){
        $('#emoption_wideslideshow').hide();
		$('#page_boxconfig').show();
        $('.wrapper').addClass('em-box-custom');
        $('.container_slideshow').removeClass('em-wide-custom');            
    }else{
        $('.wrapper').removeClass('em-box-custom');
        $('#emoption_wideslideshow').show();
		$('#page_boxconfig').hide();
        if(fullSlideshow){
            $('#wide_full').prop('checked', true); // Uncheck the checkbox
        }else{
            $('#wide_full').prop('checked', false);
        }        
        if ($('#wide_full').is(':checked')){
			$('.container_slideshow').addClass('em-wide-custom');
		}else{
			$('.container_slideshow').removeClass('em-wide-custom');
		}            
    }
};